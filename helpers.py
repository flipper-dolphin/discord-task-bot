def generate_task_list(ctx, tasks, server_id):
    task_display = "📝Task list📝"
    if server_id in tasks:
        for user, task in tasks[server_id].items():
            if task:
                task_display += (
                    f"\n• **{ctx.guild.get_member(int(user)).display_name}**"
                )
                for id, task in enumerate(task):
                    task_display += (
                        f"\n    • **{id+1}**: {task}"
                    )
            else:
                task_display += f"\n•{ctx.guild.get_member(int(user)).name}: No tasks for {ctx.guild.get_member(int(user)).name}"
    else:
        task_display += f"\nNo tasks for {ctx.guild.name}"
    return task_display

def list_user_tasks(tasks, server_id, user_id):
    task_display = ""
    if server_id in tasks:
        for id, task in enumerate(tasks[server_id][user_id]):
            if task:
                task_display += (
                    f"\n    • **{id+1}**: {task}"
                )   
            else:
                task_display += f"\nNo tasks set ❌"
    return task_display