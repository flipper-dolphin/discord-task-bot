import json
import os

import discord
from discord.ext import commands
from dotenv import load_dotenv
from helpers import generate_task_list, list_user_tasks

load_dotenv()

TOKEN = os.getenv("DISCORD_TOKEN")

intents = discord.Intents.default()
intents.message_content = True
intents.members = True

tasks = {}

try:
    with open("tasks.json", "r") as f:
        tasks = json.load(f)
except FileNotFoundError:
    print("No file found. Starting with empty dictionary.")


bot = commands.Bot(command_prefix="!", intents=intents)


@bot.command(name="task", aliases=["ass", "next"])
async def task(ctx, *args):
    """Add or remove a task for the current user."""
    task = ' '.join(args)
    server_id = str(ctx.guild.id)
    user_id = str(ctx.message.author.id)
    if server_id not in tasks:
        tasks[server_id] = {}
    if user_id not in tasks[server_id]:
        tasks[server_id][user_id] = []
    tasks[server_id][user_id].append(task)
    await ctx.reply(f'Task "{task}" added ✔️ Use "!done {len(tasks[server_id][user_id])}" to tick it off')
    print(
        f"Added task '{task}' for user '{user_id}' on server '{server_id}'")



@bot.command(name="done")
async def done(ctx, *args):
    """Add or remove a task for the current user."""
    server_id = str(ctx.guild.id)
    user_id = str(ctx.message.author.id)
    if not args:
        await ctx.reply("Please specify which task you want to complete ❌" + list_user_tasks(tasks=tasks, server_id=server_id, user_id=user_id))
        return
    
    task_to_remove = 0
    try:
        task_to_remove = int(args[0]) - 1
    except ValueError:
        await ctx.reply("Please enter a number of a task ❌" + list_user_tasks(tasks=tasks, server_id=server_id, user_id=user_id))
        return
    
    if server_id not in tasks or user_id not in tasks[server_id]:
        await ctx.reply("You don't have any tasks. ❌")
        return
    # Stopping out of range errors
    try:
        if not tasks[server_id][user_id][task_to_remove]:
            await ctx.reply("Are you sure that task exists? ❌" + list_user_tasks(tasks=tasks, server_id=server_id, user_id=user_id))
            return
    except IndexError: 
        await ctx.reply("Are you sure that task exists? ❌" + list_user_tasks(tasks=tasks, server_id=server_id, user_id=user_id))
        return
    print(
        f"Completed task '{tasks[server_id][user_id][task_to_remove]}' for user '{user_id}' on server '{server_id}'")
    task_hold = tasks[server_id][user_id][task_to_remove]
    tasks[server_id][user_id].pop(task_to_remove)
    output = ""
    if tasks[server_id][user_id]:
        output = "\nRemaining tasks 📝 "+list_user_tasks(tasks=tasks, server_id=server_id, user_id=user_id)
    if not tasks[server_id]:
        tasks.pop(server_id)
    await ctx.reply(
        f"Congrats {ctx.message.author.display_name} 🥳 {task_hold} completed! ✔️ {output}"
    )


@bot.command(name="check")
async def check(ctx):
    """Check a current users task"""
    server_id = str(ctx.guild.id)
    user_id = str(ctx.message.author.id)
    if server_id not in tasks or user_id not in tasks[server_id]:
        await ctx.reply("You don't have any tasks. ❌")
        return
    else:
        await ctx.reply(f"Your current tasks {list_user_tasks(tasks=tasks, server_id=server_id, user_id=user_id)}")


@bot.command(name="remove")
async def del_task(ctx):
    """Remove a current users task"""
    server_id = str(ctx.guild.id)
    user_id = str(ctx.message.author.id)
    if server_id not in tasks or user_id not in tasks[server_id]:
        await ctx.reply("You don't have any tasks. ❌")
        return
    else:
        await ctx.reply(
            f"Hey {ctx.message.author.display_name}, I've removed {tasks[server_id][user_id]} ✔️"
        )
        print(
            f"Removed task '{tasks[server_id][user_id]}' for user '{user_id}' on server '{server_id}'"
        )
        tasks[server_id].pop(user_id)
        if not tasks[server_id]:
            tasks.pop(server_id)

@bot.command(name="list")
async def list_task(ctx):
    server_id = str(ctx.guild.id)
    task_display = generate_task_list(ctx=ctx, tasks=tasks, server_id=server_id)
    await ctx.reply(task_display)
    print(f"Listed tasks for server '{server_id}'")

@bot.event
async def on_command_error(ctx, error):
    print("Handling error...")
    if isinstance(error, commands.UnexpectedQuoteError):
        if hasattr(ctx, 'coerced_quotes'):
            return
        if error.quote == '’':
            ctx.message.content = ctx.message.clean_content.replace("’", "'")
            ctx.coerced_quotes = True
            await bot.invoke(ctx)

@bot.event
async def on_command_completion(ctx):
    with open("tasks.json", "w") as f:
        json.dump(tasks, f)


bot.run(TOKEN)
