# Discord Task Bot



## What is this?
This bot is made for co-working discords to keep track of their member's tasks, providing accountability. 

## What do I need to know?
* The list of tasks is currently global and needs scoping to servers
* There is no persistence of tasks, if the bot goes down so does the task list 
* Not a whole lot of formatting yet

## Cool, got it, how do I run this?
There is a docker container built via GitLab CI and pushed to the GitLab container registry, currently only targeting arm64

`DISCORD_TOKEN` environment variable should be set when running it

Example docker compose is stored in the repo and is the recommended way of running the bot